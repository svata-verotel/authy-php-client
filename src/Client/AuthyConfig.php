<?php

namespace Bitsafe\Authy\Client;

class AuthyConfig
{
    private const AUTHORIZE_PATH = '/oauth2/authorize';
    private const TOKEN_PATH = '/oauth2/token';

    public function __construct(
        readonly string $server,
        readonly string $clientId,
        readonly string $clientSecret,
        readonly string $redirectUri,
        private readonly array $scopes,
        private readonly string $state,
    )
    {
    }

    public static function withRandomState(
        string $server,
        string $clientId,
        string $clientSecret,
        string $redirectUri,
        array  $scopes,
    ): self
    {
        $state = self::randomState();

        return new self(
            $server,
            $clientId,
            $clientSecret,
            $redirectUri,
            $scopes,
            $state
        );
    }

    public static function randomState(): string
    {
        return base64_encode(random_bytes(10));
    }

    public function authorizationHeader(): string
    {
        $authorization = base64_encode("{$this->clientId}:{$this->clientSecret}");
        return "Basic $authorization";
    }

    public function state(): string
    {
        return $this->state;
    }

    public function authorizationUrl(): string
    {
        $scopes = implode("%20", $this->scopes);
        $authorizePath = self::AUTHORIZE_PATH;
        return "{$this->server}$authorizePath?response_type=code&client_id={$this->clientId}&scope={$scopes}&state={$this->state}&redirect_uri={$this->redirectUri}";
    }

    public function tokenUrl(): string
    {
        return $this->server . self::TOKEN_PATH;
    }

    public function validateState(string $state): void
    {
        if ($this->state !== $state) {
            throw new AuthyException('State mismatch, have to stop because of probable CSRF attack');
        }
    }
}