<?php

namespace Bitsafe\Authy\Client;

class AuthyClient
{
    public function __construct(
        private readonly AuthyConfig $config,
        private readonly TokenFetcher $tokenFetcher
    )
    {
    }

    function fetchToken(string $code): string
    {
        return $this->tokenFetcher->token($this->config, $code);
    }

    function parseIdTokenFromStringResponse(string $responseBody): array
    {
        $jsonResponse = json_decode($responseBody, true);
        $encodedIdToken = $jsonResponse['id_token'];
        $idTokenData = base64_decode(str_replace('_', '/', str_replace('-', '+', explode('.', $encodedIdToken)[1])));
        return json_decode($idTokenData, true);
    }
}