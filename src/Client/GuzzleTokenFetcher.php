<?php

namespace Bitsafe\Authy\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class GuzzleTokenFetcher implements TokenFetcher
{

    public function token(AuthyConfig $config, string $code): string
    {
        $client = new Client();
        $request = new Request(
            'POST',
            $config->tokenUrl(),
            [
                'Authorization' => $config->authorizationHeader(),
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query([
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => $config->redirectUri,
            ])
        );

        return $client->send($request)->getBody()->getContents();
    }
}