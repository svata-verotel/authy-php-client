<?php

namespace Bitsafe\Authy\Client;

interface TokenFetcher
{
    public function token(AuthyConfig $config, string $code): string;
}