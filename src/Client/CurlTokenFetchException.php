<?php

namespace Bitsafe\Authy\Client;

class CurlTokenFetchException extends AuthyException
{
    public function __construct(
        string  $message,
        private readonly array $info,
    )
    {
        parent::__construct($message);
    }
}