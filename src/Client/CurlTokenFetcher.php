<?php

namespace Bitsafe\Authy\Client;

class CurlTokenFetcher implements TokenFetcher
{

    public function token(AuthyConfig $config, string $code): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $config->tokenUrl());
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query([
                'code' => $code,
                'redirect_uri' => $config->redirectUri,
                'grant_type' => 'authorization_code'
            ])
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Authorization: {$config->authorizationHeader()}",
            'Content-Type: application/x-www-form-urlencoded'
        ]);

        $body = curl_exec($ch);

        $info = curl_getinfo($ch);
        if ($info['http_code'] !== 200) {
            throw new CurlTokenFetchException("Token request failed, resulted in {$info['http_code']} HTTP response", $info);
        }

        return $body;
    }
}