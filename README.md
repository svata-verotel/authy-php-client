# Authy PHP Client

Use Authy in pure PHP.

## Before Start

Decide where exactly will Your app ask for data from Bitsafe, more specifically what will be the URL that will handle
`authorization_code` and will fetch `token` after a talent gives consent. This URL is called `redirectUri`.

You'll help the Prague Team B if You'll prepare following information by Yourself:
```
clientId: Your platform name, eg. "onlybits"
clientSecret: Super-secret and secure password, eg. ....not, really?
redirectUri: URL from the first step
scopes: platform/profile - if You need only Alias, ask for platform. If you are PCI compatible, ask for profile.
```

After that contact Team B to register Your platform. Platform is in role of a client for Authy, so from now on
we'll call the platform a `client`.

## Installation

Via composer.

As we're under construction, You have to specify repository URL in composer.json
```
"repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/svata-verotel/authy-php-client"
    }
]
```

And now run `composer require bitsafe/authy-php-client:dev-main`

## Coding time!

### Everything on a single page

Link and handler is on a single page. This page must be located on `redirectUri`.

```php
<?php
require_once("vendor/autoload.php");

$code = $_GET['code'] ?? null;

$config = Bitsafe\Authy\Client\AuthyConfig::withRandomState(
    server: 'http://authy:7101', //or https://staging.accounts.bitsafe.com, or https://accounts.bitsafe.com
    clientId: '...', // fill information from previous step
    clientSecret: '...',
    redirectUri: '...',
    scopes: ['openid', '...'],
);

if ($code === null) {
    echo "<a href='{$config->authorizationUrl()}'>Log in using Bitsafe</a>";
    exit;
}

assert(is_string($code));

$client = new Bitsafe\Authy\Client\AuthyClient($config, new Bitsafe\Authy\Client\CurlTokenFetcher());
// Or You can choose guzzle implementaion
//$client = new Bitsafe\Authy\Client\AuthyClient($config, new Bitsafe\Authy\Client\GuzzleTokenFetcher());

$token = $client->fetchToken($code);
$idToken = $client->parseIdTokenFromStringResponse($token);

// 🎉 NOW store the $idToken to DB!
```

### Request and handler page separated

#### Render link on any page

```php
<?php
require_once("vendor/autoload.php");

$config = Bitsafe\Authy\Client\AuthyConfig::withRandomState(
    server: 'http://authy:7101', //or https://staging.accounts.bitsafe.com, or https://accounts.bitsafe.com
    clientId: '...', // fill information from previous step
    clientSecret: '...',
    redirectUri: '...',
    scopes: ['openid', '...'],
);

echo "<a href='{$config->authorizationUrl()}'>Log in using Bitsafe</a>";
```

#### Handler page

This page must be located on `redirectUri`.

```php
<?php
require_once("vendor/autoload.php");

$code = $_GET['code'] ?? throw Exception('code must be specified in URL');
assert(is_string($code));

$config = Bitsafe\Authy\Client\AuthyConfig::withRandomState(
    server: 'http://authy:7101', //or https://staging.accounts.bitsafe.com, or https://accounts.bitsafe.com
    clientId: '...', // fill information from previous step
    clientSecret: '...',
    redirectUri: '...',
    scopes: ['openid', '...'],
);

$client = new Bitsafe\Authy\Client\AuthyClient($config, new Bitsafe\Authy\Client\CurlTokenFetcher());

$token = $client->fetchToken($code);
$idToken = $client->parseIdTokenFromStringResponse($token);

// 🎉 NOW store the $idToken to DB!
```

### Check the `state` parameter

To [Prevent Attacks and Redirect Users with OAuth 2.0 State Parameters](https://auth0.com/docs/secure/attack-protection/state-parameters)

```php
<?php

require_once("vendor/autoload.php");

$code = $_GET['code'] ?? null;

if ($code === null) {
    $state = Bitsafe\Authy\Client\AuthyConfig::randomState();
    $_SESSION['authy_oauth2_state'] = $state;
} else {
    $state = $_SESSION['authy_oauth2_state'];
}

$config = new Bitsafe\Authy\Client\AuthyConfig(
    server: 'http://authy:7101',
    clientId: 'onlybits',
    clientSecret: 'secret',
    redirectUri: 'http://onlybits:8000/client/client.php',
    scopes: ['openid', 'platform'],
    state: $state
);

if (isset($_GET['state'])) {
    $config->validateState($_GET['state']);
}

if ($code === null) {
    echo "<a href='{$config->authorizationUrl()}'>Log in using Bitsafe</a>";
    exit;
}

assert(is_string($code));

$client = new Bitsafe\Authy\Client\AuthyClient($config, new Bitsafe\Authy\Client\CurlTokenFetcher());
//$client = new Bitsafe\Authy\Client\AuthyClient($config, new Bitsafe\Authy\Client\GuzzleTokenFetcher());

$token = $client->fetchToken($code);
$idToken = $client->parseIdTokenFromStringResponse($token);

// 🎉 NOW store the $idToken to DB!
```